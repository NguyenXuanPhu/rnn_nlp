# coding: utf-8
import argparse
import torch
import torch.onnx
from utils.data_utils import Vocab, Txtfile, Data2tensor, SaveloadHP, seqPAD, PAD
from utils.core_nns import RNNModel
from model import Languagemodel

# Set the random seed manually for reproducibility.
Data2tensor.set_randseed(1234)

PAD = u"<PAD>"
UNK = u"<UNK>"
SOS = u"<s>"
EOS = u"</s>",

class predict(object):
    def __init__(self, args):
        self.args = args
        self.vocab = args.vocab
        self.device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
        self.ntokens = len(self.args.vocab.w2i)
        self.model = RNNModel(args.model, self.ntokens, args.emsize, args.nhid,
                              args.nlayers, args.dropout, args.tied, args.bidirect).to(self.device)

    def load_model(self, type_model):
        if (type_model==1):
            self.model.load_state_dict(torch.load("results/lm.args"))
            self.model.to(self.device)
        else:
            self.model.load_state_dict(torch.load("results/lm.m"))

    def word_out(self, text):
        list_words = text.lower().split()
        print(list_words)
        w2i = self.vocab.w2i
        word_ids = []
        for word in list_words:
            if word in w2i:
                word_ids += [w2i[word]]
            else:
                word_ids += [w2i[UNK]]

        # Convert to Tensor
        word_tensor = torch.tensor([word_ids], dtype=torch.long, device=self.device)
        hidden = self.model.init_hidden(word_tensor.size(0))
        output, hidden = self.model(word_tensor, hidden)

        # Convert a Torch Tensor to number
        label_probs, label_preds = self.model.inference(output, 1)
        print(label_preds)

        # Convert number to words
        i2w = self.vocab.i2w
        word_output = [i2w[label_pred[0]] for label_pred in label_preds.tolist()[0]]
        print(word_output)

        return word_output

    def rev_gen(self, text):
        list_words = []
        list_words += [text]
        i = 0
        #while (1):
        for i in range(0, 10):
            word_output = predict.word_out(list_words[i])
            list_words += word_output
            #i += 1
            print(list_words)
            if word_output == u"</s>":
                break
        return list_words

    #def word_pred(self, available_word):

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Language Model')

    parser.add_argument('--train_file', help='Trained file', default="./dataset/train.small.txt", type=str)

    parser.add_argument('--dev_file', help='Trained file', default="./dataset/val.small.txt", type=str)

    parser.add_argument('--test_file', help='Tested file', default="./dataset/test.small.txt", type=str)

    parser.add_argument("--wl_th", type=int, default=-1, help="Word threshold")

    parser.add_argument("--cutoff", type=int, default=1, help="Prune words occurring <= wcutoff")

    parser.add_argument("--se_words", action='store_false', default=True, help="Start-end padding flag at word")

    parser.add_argument("--allow_unk", action='store_false', default=True, help="allow using unknown padding")

    parser.add_argument('--model', type=str, default='RNN_TANH',
                        help='type of recurrent net (RNN_TANH, RNN_RELU, LSTM, GRU)')

    parser.add_argument('--emsize', type=int, default=16, help='size of word embeddings')

    parser.add_argument('--nhid', type=int, default=32, help='number of hidden units per layer')

    parser.add_argument('--nlayers', type=int, default=1, help='number of layers')

    parser.add_argument('--bidirect', action='store_true', default=False, help='bidirectional flag')

    parser.add_argument('--lr', type=float, default=20, help='initial learning rate')

    parser.add_argument('--clip', type=float, default=0.25, help='gradient clipping')

    parser.add_argument('--epochs', type=int, default=8, help='upper epoch limit')

    # batch_size: here, it is the number of sentence being training at the same time
    # The actual value of batch_size = batch_size * bptt
    parser.add_argument('--batch_size', type=int, default=16, metavar='N', help='batch size')

    parser.add_argument('--bptt', type=int, default=32, help='sequence length')

    parser.add_argument('--es', type=int, default=2, help='Early stopping criterion')

    parser.add_argument('--dropout', type=float, default=0.5, help='dropout applied to layers (0 = no dropout)')

    parser.add_argument('--tied', action='store_true', help='tie the word embedding and softmax weights')

    parser.add_argument('--trained_model', type=str, default='./results/lm.m', help='path to save the final model')

    parser.add_argument('--model_args', type=str, default='./results/lm.args', help='path to save the model argument')

    parser.add_argument("--use_cuda", action='store_true', default=False, help="GPUs Flag (default False)")

    args = parser.parse_args()

    args = Languagemodel.build_data(args)

    predict = predict(args)
    predict.load_model(type_model=0)
    predict.rev_gen("you")



